package arrvarro.OperacionesConFicheros;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Álvaro Paradas Borrego
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Persona {
    @XmlElement
    private int id;
    @XmlElement
    private String AString;
    @XmlElement
    private String nombre;
    @XmlElement
    private String apellidos;
    @XmlElement
    private String sexo;
    @XmlElement
    private String DNI;
    @XmlElement
    private String calle;
    @XmlElement
    private String codigoPostal;
    @XmlElement
    private String poblacion;
    @XmlElement
    private String telefonoFijo;
    @XmlElement
    private String telefonoMovil;
    @XmlElement
    private Date fechaNacimiento;  
    
    // metodo constructor
    public Persona(String nombre, String apellidos, String sexo, String DNI, String calle, String codigoPostal, 
            String poblacion, String telefonoFijo, String telefonoMovil, Date fechaNacimiento) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.sexo = sexo;
        this.DNI = DNI;
        this.calle = calle;
        this.codigoPostal = codigoPostal;
        this.poblacion = poblacion;
        this.telefonoFijo = telefonoFijo;
        this.telefonoMovil = telefonoMovil;
        this.fechaNacimiento = fechaNacimiento;
        this.id = id;
        this.AString = AString;
    } 
    
    // metodo constructor vacio
    public Persona() {
    }    

    // metodos get y set
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getTelefonoFijo() {
        return telefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getAString() {
        return AString;
    }

    public void setAString(String AString) {
        this.AString = AString;
    }
}