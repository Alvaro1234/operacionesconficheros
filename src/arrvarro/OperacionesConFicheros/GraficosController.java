package arrvarro.OperacionesConFicheros;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;


/**
 * FXML Controller class
 *
 * @author Álvaro Paradas Borrego 
 */

public class GraficosController implements Initializable {

    private TableView<Persona> tableView;
    private Persona personasSelected;
    
    @FXML
    private TextField TextFieldNombre;
    @FXML
    private TextField TextFieldApellidos;
    @FXML
    private TextField TextFieldSexo;
    @FXML
    private TextField TextFieldDNI;
    @FXML
    private TextField TextFieldCalle;
    @FXML
    private TextField TextFieldCodigoPostal;
    @FXML
    private TextField TextFieldPoblacion;
    @FXML
    private TextField TextFieldTelefonoFijo;
    @FXML
    private TextField TextFieldTelefonoMovil;
    @FXML
    private DatePicker DatePicker;
    @FXML
    private Button buttonGuardar;
    @FXML
    private Button buttonCancelar;
    
    ListaIndividuos objetoListaIndividuos;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        // boton para guardar los datos modificados de la persona
        buttonGuardar.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                try {
                    // actualizamos el campo String
                    personasSelected.setNombre(TextFieldNombre.getText());
                    personasSelected.setApellidos(TextFieldApellidos.getText());
                    personasSelected.setSexo(TextFieldSexo.getText());
                    personasSelected.setDNI(TextFieldDNI.getText());
                    personasSelected.setCalle(TextFieldCalle.getText());
                    personasSelected.setCodigoPostal(TextFieldCodigoPostal.getText());
                    personasSelected.setPoblacion(TextFieldPoblacion.getText());
                    personasSelected.setTelefonoFijo(TextFieldTelefonoFijo.getText());
                    personasSelected.setTelefonoMovil(TextFieldTelefonoMovil.getText());
                    
                    // actualizar el dampo DatePicker
                    personasSelected.setFechaNacimiento(UtilJavaFx.getDateFromDatePicket(DatePicker));
                    
                    // salimos de la pantalla de modificacion y pasamos a la principal
                    objetoListaIndividuos.deleteGraficosController();
                } catch (IOException ex) {
                    Logger.getLogger(GraficosController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        buttonCancelar.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                try {            
                    objetoListaIndividuos.deleteGraficosController();
                } catch (IOException ex) {
                    Logger.getLogger(GraficosController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }    
    
    public void setListaIndividuos(ListaIndividuos objetoListaIndividuos){
        this.objetoListaIndividuos = objetoListaIndividuos;
    }
    
    // metodo para pasarme de la clase ListaIndividuos el tableView
    public void setTableView(TableView<Persona> tableView) {
        this.tableView = tableView;
    }
    
   
    
    // Metodo para mostrar más detalles del individuo seleccionado
    public void getMoreDetails(){
        // guardamos en una variable el individuo seleccionado
        personasSelected = tableView.getSelectionModel().getSelectedItem();
        // reyenamos los TextFields 
        TextFieldNombre.setText(personasSelected.getNombre());
        TextFieldApellidos.setText(personasSelected.getApellidos());
        TextFieldSexo.setText(personasSelected.getSexo());
        TextFieldDNI.setText(personasSelected.getDNI());
        TextFieldCalle.setText(personasSelected.getCalle());
        TextFieldCodigoPostal.setText(personasSelected.getCodigoPostal());
        TextFieldPoblacion.setText(personasSelected.getPoblacion());
        TextFieldTelefonoFijo.setText(personasSelected.getTelefonoFijo());
        TextFieldTelefonoMovil.setText(personasSelected.getTelefonoMovil());
        UtilJavaFx.setDateInDatePicker(DatePicker, personasSelected.getFechaNacimiento());                  
    }
    
    

    
}
