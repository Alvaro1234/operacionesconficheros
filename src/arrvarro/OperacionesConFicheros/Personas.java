
package arrvarro.OperacionesConFicheros;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Álvaro Paradas Borrego
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Personas {
    private List<Persona> listaPersonas;
    
    public Personas(){
        listaPersonas = new ArrayList();
    }
    
    public List<Persona> getListaPersonas() {
        return listaPersonas;
    }
}

