package arrvarro.OperacionesConFicheros;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


/**
 *
 * @author Álvaro Paradas Borrego
 */

public class OperacionesConFicheros {

    // guardamos el nombre para declarar posteriormente el arrayList   
    public static ArrayList<Persona> listaIndividuos;
    // declaramos el objeto persona
    public static Persona persona;
    // declaramos un objeto personas
    public static Personas personas = new Personas();
    // creamos un objeto Logger
    private static final Logger LOG = Logger.getLogger(OperacionesConFicheros.class.getName());    
    public static void leerLineas(){        
        String nombreFichero = "agenda.csv";
            //Declarar una variable BufferedReader
            BufferedReader br = null;
            
            // formato de fecha DATE
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yy");
            // creamos el arrayList
            listaIndividuos = new ArrayList();
            
            
            try {
               //Crear un objeto BufferedReader al que se le pasa 
               //un objeto FileReader con el nombre del fichero
               br = new BufferedReader(new FileReader(nombreFichero));
               //Leer la primera línea, guardando en un String
               String texto = br.readLine();
               // donde se dividen las lineas
               String[] datos;
               // creamos la variable de formato date para usarla en el array
                       
               //Repetir mientras no se llegue al final del fichero
               while(texto != null)
               {
                   
                   // leemos y separamos las lineas
                   //System.out.println(texto);
                   datos = texto.split(";");
                   
                   
                   Date fecha = null;
            try {
                fecha = formatoDelTexto.parse(datos[9]);

                } catch (ParseException ex) {

                    ex.printStackTrace();

                    }            
                
               persona = new Persona (datos[0], datos[1], datos[2], datos[3], datos[4], datos[5], datos[6], datos[7], datos[8], fecha);
                   //System.out.println(persona.getNombre() + " " + persona.getApellidos());
                   // añadimos el objeto persona a la lista del ArrayList
                   listaIndividuos.add(persona);
                   personas.getListaPersonas().add(persona);
                   texto = br.readLine();                   
               }
                for(int i=0; i<listaIndividuos.size(); i++){
                    persona = listaIndividuos.get(i);
                    //LOG.info(persona.getNombre() + " " + persona.getApellidos());                    
                }
                
                // llenamos la lista de Personas
                //personas.getListaPersonas().add(persona);
               
            }
            catch (FileNotFoundException e) {
                System.out.println("Error: Fichero no encontrado");
                System.out.println(e.getMessage());
            }
            catch(Exception e) {
                System.out.println("Error de lectura del fichero");
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
            finally {
                try {
                    if(br != null)
                        br.close();
                }
                catch (Exception e) {
                    System.out.println("Error al cerrar el fichero");
                    System.out.println(e.getMessage());
                }
            } 
        
    }
    
    // metodo para mostrar la lista de las persona que se han obtenido
    public static ArrayList<Persona> getListaIndividuos(){
        return listaIndividuos;
    }
    
    public void getXML() {
        try {
            // Se indica el nombre de la clase que contiene la lista de objetos
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // Indicar que se desea generar el xml con saltos de línea y tabuladores
            //  para facilitar su lectura
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            
            
            // Generar XML mostrándolo por la salida estándar
            jaxbMarshaller.marshal(personas, System.out);
            
            // Generar XML guardándolo en un archivo local
            BufferedWriter bw = new BufferedWriter(new FileWriter("personas.xml"));
            jaxbMarshaller.marshal(personas, bw);
        } catch (JAXBException ex) {
            Logger.getLogger(OperacionesConFicheros.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OperacionesConFicheros.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void getIndividuosConXML(){
        try {
            BufferedReader br = new BufferedReader(new FileReader("personas.xml"));
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            personas = (Personas) jaxbUnmarshaller.unmarshal(br);
            for (Persona persona : personas.getListaPersonas()) {
                System.out.println("Nombre: " + persona.getNombre());
                System.out.println("Apellidos: " + persona.getApellidos());
                System.out.println("Sexo: " + persona.getSexo());
                System.out.println("DNI: " + persona.getDNI());                
            }
        } catch (JAXBException ex) {
            Logger.getLogger(OperacionesConFicheros.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OperacionesConFicheros.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void getIndividuosConURL(){
            try {
            // Crear objeto JAXB para interpretar objetos 'Items' desde XML
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            // Generar lista de objetos desde XML descargado de URL
            URL url = new URL("http://misitiowebdeprogramacion.hol.es/personas.xml");
            InputStream is = url.openStream();
            Personas personas = (Personas) jaxbUnmarshaller.unmarshal(is);
            
            // Mostrar el contenido de la lista obtenida
            for (Persona persona : personas.getListaPersonas()) {
                System.out.println("Nombre: " + persona.getNombre());
                System.out.println("Apellidos: " + persona.getApellidos());
                System.out.println("Sexo: " + persona.getSexo());
                System.out.println("DNI: " + persona.getDNI());
            }
        } catch (Exception ex) {
            Logger.getLogger(OperacionesConFicheros.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void getAtenderPeticionesGet() throws MalformedURLException, IOException, JAXBException{
        // creamos una conexion con la URL del Servlet
        String strConexion = "http://localhost:8084/WebApplication1/SimpleQuerySample"; // insertar la URL del Servlet
        URL url = new URL(strConexion);
        URLConnection uc = url.openConnection();
        HttpURLConnection conn = (HttpURLConnection) uc;
        // realizamos la conexion para poder enviar y recivir informacion en formato XML
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-type", "text/xml");
        // Se va a realizar una petición con el método GET
        conn.setRequestMethod("GET");
        // Ejecutamos la conexion y obtenemos una respuesta
        InputStreamReader isr = new InputStreamReader(conn.getInputStream());
        // Procesamos la respuesta en formato XML y la obtendremos en forma de un objeto de la clase Personas
        JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        Object response = jaxbUnmarshaller.unmarshal(isr);
        isr.close();
        // Convertimos a la clase personas el objeto obtenido en la respuesta
        personas = (Personas)response;
        // Mostraré por la salida estandar algunos datos para probar que funciona correctamente todo
        for(Persona persona : personas.getListaPersonas()) {
            System.out.println(persona.getNombre() + ", " + persona.getApellidos() + ", " + persona.getId());
        }
    }
    
    public Personas getPersonas(){
        return personas;
    }
        
    public void getAtenderPeticionPost() throws MalformedURLException, IOException, JAXBException{
        String strConnection = "http://localhost:8084/WebApplication1/SimpleQuerySample"; // insertar la URL del servlet
        URL url = new URL(strConnection);
        URLConnection uc = url.openConnection();
        HttpURLConnection conn = (HttpURLConnection) uc;
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-type", "text/xml");  
        // Se va a realizar la peticion con el metod post
        conn.setRequestMethod("POST");
        // Se va a generar un objeto con contenido aleatorio, que será el que se envie al servidor
        Persona pesona = new Persona();
        Random random = new Random();
        persona.setId(random.nextInt(1000)); // le damos un id aleatorio a la persona
//        String cadenaAleatoria = "";
//        for(int i=0; i<10; i++){
//            cadenaAleatoria += (char)('A' + random.nextInt(26));
//        }
//        pesona.setAString(cadenaAleatoria);
        /*creo un objeto de la clase personas que va a almacenar dicho objeto, 
        se hace esto para usar siempre la clase personas al generar un XML*/
        Personas personas = new Personas();
        personas.getListaPersonas().add(pesona);
        // preparamos el objeto personas para transformarlo a XML y subirlo al servidor
        JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.marshal(personas, conn.getOutputStream());
        // ejecutamos la conexion y obtenemos una respuesta
        InputStreamReader isr = new InputStreamReader(conn.getInputStream());
        LOG.info("el metodo getAtenderPeticionInsert() ha llegado hasta el final del codigo y se supone que ha añadido"
                + " " + "informacion aleatoria al servlet"); 
    }  
    
}
