
package arrvarro.OperacionesConFicheros;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javafx.scene.control.DatePicker;


/**
 *
 * @author Álvaro Paradas Borrego
 */

public class UtilJavaFx {
    public static void setDateInDatePicker(DatePicker datePicker, Date date) {
        if(date != null) {
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(); 
            datePicker.setValue(localDate);
        }
    }
    
    public static Date getDateFromDatePicket(DatePicker datePicker) {
        LocalDate localDate = datePicker.getValue();
        if(localDate != null) {
            Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
            Date date = Date.from(instant);  
            return date;
        } else {
            return null;
        }
    }
}
