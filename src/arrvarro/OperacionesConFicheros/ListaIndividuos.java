package arrvarro.OperacionesConFicheros;

import static arrvarro.OperacionesConFicheros.OperacionesConFicheros.personas;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import static javafx.application.Application.launch;
import javafx.scene.layout.StackPane;
import javax.xml.bind.JAXBException;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;


/**
 *
 * @author Álvaro Paradas Borrego
 */

public class ListaIndividuos extends Application{
  
    // creamos un objeto de la clase OperacionesConFicheros
    OperacionesConFicheros ocf = new OperacionesConFicheros();
    private TableView<Persona> table = new TableView<Persona>();
    private ObservableList<Persona> data = null;
    Personas personas = new Personas();
    
    
    StackPane root = new StackPane();
    Parent parentScreen1;
        
   
    public static void main(String[] args) {
        launch(args);
        // launch llama al start, lanzamiento de la aplicacion
        
    }
    private static final java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(OperacionesConFicheros.class.getName());
 
    public void start(Stage stage) throws IOException, MalformedURLException, JAXBException {      
        
        // cargar la lista
        //ocf.leerLineas();
        // cargamos las personas desde el servidor
        ocf.getAtenderPeticionesGet();
        // meter por parametro lista individuos
        //data = FXCollections.observableArrayList(ocf.getListaIndividuos());
        
        data = FXCollections.observableArrayList(ocf.getPersonas().getListaPersonas());
        
        // mostar pantalla datos del array paso 1
        //OperacionesConFicheros ocf = new OperacionesConFicheros();
        //ocf.leerLineas();
        //ocf.getListaIndividuos();
        
        
        
        Scene scene = new Scene(root);
        stage.setTitle("Lista de Individuos");
        stage.setWidth(620);
        stage.setHeight(600);
 
        final Label label = new Label("Lista de Individuos");
        label.setFont(new Font("Arial", 20));
 
        table.setEditable(true);
 
        TableColumn nombre = new TableColumn("Nombre");
        nombre.setMinWidth(150);
        nombre.setCellValueFactory(new PropertyValueFactory<Persona, String>("Nombre"));
 
        TableColumn apellidos = new TableColumn("Apellidos");
        apellidos.setMinWidth(200);
        apellidos.setCellValueFactory(new PropertyValueFactory<Persona, String>("Apellidos"));
 
        TableColumn sexo = new TableColumn("Sexo");
        sexo.setMinWidth(50);
        sexo.setCellValueFactory(new PropertyValueFactory<Persona, String>("Sexo"));
 
        // creamos los botones con unas imagenes
        // boton añadir individuo
        Image imagenAñadir = new Image(getClass().getResourceAsStream("/imagenes/ic_person_add.png"));
        Button botonAñadir = new Button("Añadir", new ImageView(imagenAñadir));
        
        // boton editar individuo seleccionado
        Image imagenEditar = new Image(getClass().getResourceAsStream("/imagenes/ic_edit.png"));
        Button botonEditar = new Button("Editar", new ImageView(imagenEditar));
        
        // boton eliminar un individuo seleccionado
        Image imagenEliminar = new Image(getClass().getResourceAsStream("/imagenes/ic_delete.png"));
        Button botonEliminar = new Button("Eliminar", new ImageView(imagenEliminar));
                
        // le paso los individuos de la lista de individuos
        table.setItems(data);
        table.getColumns().addAll(nombre, apellidos, sexo);
        botonEliminar.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                // guardamos la persona seleccionada en una variable
                Persona p = table.getSelectionModel().getSelectedItem();                
                if (p!=null){                    
                    try {
                        //OperacionesConFicheros.listaIndividuos.remove(p);
                        //data.remove(p);
                        String strConexion = "http://localhost:8084/WebApplication1/SimpleQuerySample"; 
                        URL url = new URL(strConexion);
                        URLConnection uc = url.openConnection();
                        HttpURLConnection conn = (HttpURLConnection) uc;
                        conn.setDoOutput(true);
                        personas.getListaPersonas().add(p);
                        JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
                        Marshaller jaxbMarshaller = jaxbContext.createMarshaller(); 
                        conn.setRequestMethod("DELETE");
                        jaxbMarshaller.marshal(personas, conn.getOutputStream());
                        data.remove(p);
                        LOG.info("Se ha borrado a un individuo");
                    } catch (JAXBException ex) {
                        Logger.getLogger(ListaIndividuos.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(ListaIndividuos.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    // ventana de notificacion
                    Alert alert = new Alert(AlertType.INFORMATION, "Debes seleccionar un individuo");
                    // cambiamos titulo a la ventana de notificacion
                    alert.setTitle("¡¡ATENCIÓN!!");
                    // cambiamos el contenido del Tipo de informacion
                    alert.setHeaderText("INFORMACIÓN");
                    // ponemos un icono a la ventana de notificacion
//                    Image image1 = new Image(this.getClass().getResource("/imagenes/notificacion.png").toString());
//                    ImageView imageView = new ImageView(image1);
//                    alert.setGraphic(imageView);                    
                    
                    // mostramos en la pantalla la ventana de notificacion
                    alert.showAndWait();
                    LOG.info("No se ha realizado el borrado porque no ha seleccionado un individuo");
                }
            }
        });
        
         
    
        
        // boton para modificar datos
        botonEditar.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Persona p = table.getSelectionModel().getSelectedItem();                
                if (p!=null){                    
                    showGraficosController(); 
                    LOG.info("Se ha modificado a un individuo");
                } else {
                    // ventana de notificacion
                    Alert alert = new Alert(AlertType.INFORMATION, "Debes seleccionar un individuo");
                    // cambiamos titulo a la ventana de notificacion
                    alert.setTitle("¡¡ATENCIÓN!!");
                    // cambiamos el contenido del Tipo de informacion
                    alert.setHeaderText("INFORMACIÓN");
                    // ponemos un icono a la ventana de notificacion
//                    Image image1 = new Image(this.getClass().getResource("/imagenes/notificacion.png").toString());
//                    ImageView imageView = new ImageView(image1);
//                    alert.setGraphic(imageView);                    
                    
                    // mostramos en la pantalla la ventana de notificacion
                    alert.showAndWait();
                    LOG.info("No se ha realizado la modificacion porque no ha seleccionado un individuo");
                }
                               
            }
        });
        
        botonAñadir.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                //showGraficosController();
            }
        });
 
        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label, table, botonAñadir, botonEditar, botonEliminar);
 
        ((StackPane) scene.getRoot()).getChildren().addAll(vbox);
 
        stage.setScene(scene);
        stage.show();
        
       
            // generamos el archivo XML
            //ocf.getXML();
            // cargamos la lista de los individuos a partir de un documento XML
            //ocf.getIndividuosConXML();
            // cargamos el XML desde una URL de un servidor donde esta alojado
            //ocf.getIndividuosConURL();
            // sacamos datos extraidos de la conexion con el servlet desde la aplicacion web
            //ocf.getAtenderPeticionesGet();
            //ocf.getAtenderPeticionPost();

    }
    
    
    
    public static void mostrarIndividuos(ArrayList<Persona> listaIndividuos){
        
//        for(int i=0; i<listaIndividuos.size(); i++) {
//            Persona p = listaIndividuos.get(i);
//            
//        }
        
        for (Persona p: listaIndividuos)
            System.out.println(p.getNombre() + " " + p.getDNI());
    }

    
    private void showGraficosController() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Graficos.fxml"));
            parentScreen1 = loader.load();
            GraficosController graficosController = loader.getController();
            graficosController.setListaIndividuos(this);
            // Pasar una referencia a la tabla para que Screen1 pueda acceder a su contenido
            graficosController.setTableView(table);
            // Rellenar Screen1 con los datos del objeto actual
            graficosController.getMoreDetails();
            // Mostrar los elementos de Screen1 colgándolo del contenedor raiz
            root.getChildren().add(parentScreen1);
        } catch (IOException ex) {
            Logger.getLogger(ListaIndividuos.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    public void deleteGraficosController() throws IOException{
        // actualizamos la pantalla principal despues de hacer las modificaciones de un individuo
        int posicionIndividuoSeleccionado = table.getSelectionModel().getSelectedIndex();
        Persona p = table.getSelectionModel().getSelectedItem();
        table.getItems().set(posicionIndividuoSeleccionado, p);
        root.getChildren().remove(parentScreen1);
    }

}


