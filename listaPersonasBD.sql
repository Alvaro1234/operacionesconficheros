drop database if exists listaPersonas;
create database if not exists listaPersonas;
use listaPersonas;
drop table if exists persona;
create table if not exists persona(
id int  unsigned auto_increment primary key,
nombre char(30),
apellidos char(60),
sexo char(1),
DNI char(9),
calle char(30),
codigoPostal char(5),
poblacion char(30),
telefonoFijo char(9),
telefonoMovil char(9),
fechaNacimiento date
);

insert into persona values 
(1, 'Álvaro', 'Paradas Borrego', 'M', '44053812A', 'José Maria Pemán', '11600', 'Ubrique', '956463815', '638487579', '1997-10-12'),
(2, 'Pepe', 'Bargas Sanchez', 'M', '44053452A', 'José Maria Pemán', '11600', 'Ubrique', '956463815', '638487579', '1997-10-12');